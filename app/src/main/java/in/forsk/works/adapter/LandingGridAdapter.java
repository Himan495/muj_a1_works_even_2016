package in.forsk.works.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.forsk.works.R;
import in.forsk.works.wrapper.LandingGridWrapper;

/**
 * Created by Saurabh on 2/20/2016.
 */
public class LandingGridAdapter extends BaseAdapter {

    private final static String TAG = LandingGridAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<LandingGridWrapper> mObjList;
    private LayoutInflater inflater;

    private ViewHolder holder;

    public LandingGridAdapter(Context context, ArrayList<LandingGridWrapper> mObjList) {

        this.context = context;
        this.mObjList = mObjList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mObjList.size();
    }

    @Override
    public Object getItem(int position) {
        return mObjList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.landing_grid_cell, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        LandingGridWrapper mObj = mObjList.get(position);

        holder.idTv.setText(mObj.id);
        holder.nameTv.setText(mObj.name);

        try {
            int drawable_identifier = context.getResources().getIdentifier("muj_" + mObj.id.toLowerCase(), "drawable", context.getPackageName());
            if (drawable_identifier != 0) {
                holder.imageView.setImageResource(drawable_identifier);
            } else {
                holder.imageView.setImageResource(R.drawable.placeholder);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public static class ViewHolder {
        TextView idTv, nameTv;
        ImageView imageView;

        public ViewHolder(View v) {
            idTv = (TextView) v.findViewById(R.id.textView1);
            nameTv = (TextView) v.findViewById(R.id.textView);
            imageView = (ImageView) v.findViewById(R.id.imageView);
        }
    }
}
